package client;


import javax.swing.*;
import javax.swing.border.EmptyBorder;


import java.awt.event.ActionListener;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	CatalogView is a JFrame which contains the UI elements of the Client application.
 */
public class ComputeView extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textYear;
	private JTextField textEngineCapacity;
	private JTextField textPurchasingPrice;
	private JButton btnTax;
	private JButton btnPrice;
	private JTextArea textArea;

	public ComputeView() {
		setTitle("HTTP Protocol simulator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblInsertCarYear= new JLabel("Insert car year");
		lblInsertCarYear.setBounds(10, 11, 120, 14);
		contentPane.add(lblInsertCarYear);

		JLabel lblInsertEngineCapacity = new JLabel("Insert engine capacity");
		lblInsertEngineCapacity.setBounds(10, 36, 160, 14);
		contentPane.add(lblInsertEngineCapacity);

		JLabel lblInsertPurchasingPrice= new JLabel("Insert purchasing price");
		lblInsertPurchasingPrice.setBounds(10, 61, 160, 14);
		contentPane.add(lblInsertPurchasingPrice);

		textYear = new JTextField();
		textYear.setBounds(150, 11, 86, 20);
		contentPane.add(textYear);
		textYear.setColumns(10);

		textEngineCapacity = new JTextField();
		textEngineCapacity.setBounds(150, 36, 86, 20);
		contentPane.add(textEngineCapacity);
		textEngineCapacity.setColumns(10);

		textPurchasingPrice = new JTextField();
		textPurchasingPrice.setBounds(150, 61, 86, 20);
		contentPane.add(textPurchasingPrice);
		textPurchasingPrice.setColumns(10);

		btnTax = new JButton("TAX");
		btnTax.setBounds(10, 132, 120, 23);
		contentPane.add(btnTax);

		btnPrice = new JButton("SELLING PRICE");
		btnPrice.setBounds(10, 164, 120, 23);
		contentPane.add(btnPrice);

		textArea = new JTextArea();
		textArea.setBounds(235, 131, 171, 120);
		contentPane.add(textArea);
	}

	public void addBtnTaxActionListener(ActionListener e) {
		btnTax.addActionListener(e);
	}

	public void addBtnPriceActionListener(ActionListener e) {
		btnPrice.addActionListener(e);
	}


	public String getYear() {
		return textYear.getText();
	}

	public String getEngineCapacity() {
		return textEngineCapacity.getText();
	}

	public String getPurchasingPrice() {
		return textPurchasingPrice.getText();
	}

	public void printCar(double c) {
		textArea.setText(String.valueOf(c));
	}

	public void clear() {
		textYear.setText("");
		textEngineCapacity.setText("");
		textPurchasingPrice.setText("");
	}
}

