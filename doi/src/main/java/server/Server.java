package server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import rpc.ISellingService;
import rpc.SellingServices;

import java.rmi.RemoteException;


public class Server {
	   public Server() {} 
	   public static void main(String args[]) { 
	      try { 
	         // Instantiating the implementation class 
	         SellingServices obj = new SellingServices(); 
	         LocateRegistry.createRegistry(1099);
	    
	         // Exporting the object of implementation class  
	         // (here we are exporting the remote object to the stub) 
	         ISellingService stub = (ISellingService) UnicastRemoteObject.exportObject(obj, 0);  
	         
	         // Binding the remote object (stub) in the registry 
	         Registry registry = LocateRegistry.getRegistry(); 
	         
	         registry.bind("ISellingService", stub);  
	         System.err.println("Server ready"); 
	      } catch (Exception e) { 
	         System.err.println("Server exception: " + e.toString()); 
	         e.printStackTrace(); 
	      } 
	   } 
}
