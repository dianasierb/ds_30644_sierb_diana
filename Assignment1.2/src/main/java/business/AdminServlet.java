package business;

import persistence.DAO.CityDAOImpl;
import persistence.DAO.FlightDAOImpl;
import persistence.DAO.HibernateConfigs;
import persistence.model.City;
import persistence.model.Flight;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class AdminServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        FlightDAOImpl flightDAO = new FlightDAOImpl();
        List<Flight> listOfFlights = flightDAO.findAllFlights();

        PrintWriter print = response.getWriter();
        String docType = "<!doctype html>\n";
        String output = docType + "<html>\n" +"<body background=http://adventureflight.ph/wp-content/uploads/2015/12/airplane-facts.jpg>"+ "<head><title> All flights </title></head>\n" + "<body>\n"
                + "<table><tr><th>Flight Id</th><th>Airplane type</th><th>Departure City</th><th>Arival City</th>"
                + "<th>Departure Date</th<th>Arrival Date</th></tr>";

        for (Flight flight :
                listOfFlights) {
            int flightId = flight.getId();
            String airplaneType = flight.getAirplaneType();
            String arrivalCity = flight.getArrivalCity().getName();
            String departureCity = flight.getDepartureCity().getName();
            String arrivalDateHour = flight.getArrivalDateHour();
            String departureDateHour = flight.getDepartureDateHour();
            String rows = "<tr><td>" + flightId + "</td>" + "<td>" + airplaneType + "</td>" + "<td>" + departureCity + "</td>"
                    + "<td>" + arrivalCity + "</td>" + "<td>" + arrivalDateHour + "</td>"
                    + "<td>" + departureDateHour + "</td></tr>";
            output += rows;
        }
        output += "</table></body></html>";
        print.println(output);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        if (request.getParameter("flightId") != null) {
            doPut(request, response);
        } else if (request.getParameter("deleteFlightId") != null) {
            doDelete(request, response);
        } else {

            String airplaneType = request.getParameter("airplaneType");
            String arrivalCity = request.getParameter("arivalCity");
            String departureCity = request.getParameter("departureCity");
            String arrivalDateHour = request.getParameter("arivalDateHour");
            String departureDateHour = request.getParameter("departureDateHour");

            FlightDAOImpl flightDAO = new FlightDAOImpl();
            CityDAOImpl cityDAO = new CityDAOImpl();
            City arrCity = new City();
            City depCity = new City();
            if (cityDAO.findCityByName(arrivalCity) == null) {
                arrCity.setName(arrivalCity);
                cityDAO.addCity(arrCity);
            } else {
                arrCity = cityDAO.findCityByName(arrivalCity);
            }

            if (cityDAO.findCityByName(departureCity) == null) {
                depCity.setName(departureCity);
                cityDAO.addCity(depCity);
            } else {
                depCity = cityDAO.findCityByName(departureCity);
            }

            Flight flight = new Flight(airplaneType, departureDateHour, arrivalDateHour, depCity, arrCity);

            flightDAO.addFlight(flight);
            response.sendRedirect(request.getContextPath() + "/admin.html");
        }
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        FlightDAOImpl flightDAO = new FlightDAOImpl();

        int flightId = Integer.parseInt(request.getParameter("flightId"));
        String airplaneType = request.getParameter("airplaneType");
        String arrivalCity = request.getParameter("arivalCity");
        String departureCity = request.getParameter("departureCity");
        String arrivalDateHour = request.getParameter("arivalDateHour");
        String departureDateHour = request.getParameter("departureDateHour");

        Flight editedFlight = flightDAO.findFlightById(flightId);

        if (!airplaneType.isEmpty()) {
            editedFlight.setAirplaneType(airplaneType);
        }

        if (!departureDateHour.isEmpty()) {
            editedFlight.setDepartureDateHour(departureDateHour);
        }

        if (!arrivalDateHour.isEmpty()) {
            editedFlight.setArrivalDateHour(arrivalDateHour);
        }

        if (verifyCitiesForEditedFlight(arrivalCity) != null) {
            editedFlight.setArrivalCity(verifyCitiesForEditedFlight(arrivalCity));
        }

        if (verifyCitiesForEditedFlight(departureCity) != null) {
            editedFlight.setDepartureCity(verifyCitiesForEditedFlight(departureCity));
        }

        flightDAO.update(editedFlight);
        response.sendRedirect(request.getContextPath() + "/admin.html");
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        HibernateConfigs.getConfig();

        FlightDAOImpl flightDAO = new FlightDAOImpl();
        Flight flight = flightDAO.findFlightById(Integer.valueOf(request.getParameter("deleteFlightId")));
        flightDAO.deleteFlight(flight);
        response.sendRedirect("admin.html");

    }


    private City verifyCitiesForEditedFlight(String cityName) {
        CityDAOImpl cityDAO = new CityDAOImpl();
        City city = new City();
        if (!cityName.isEmpty()) {
            if (cityDAO.findCityByName(cityName) == null) {
                city.setName(cityName);
                cityDAO.addCity(city);
            } else {
                city = cityDAO.findCityByName(cityName);
            }
            return city;
        }
        return null;
    }

}
