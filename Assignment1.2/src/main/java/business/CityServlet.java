package business;

import org.json.JSONObject;
import persistence.DAO.CityDAOImpl;
import persistence.model.City;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Dell on 11/7/2017.
 */
public class CityServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String selectedCity = request.getParameter("city");

        CityDAOImpl cityDAO = new CityDAOImpl();
        City city = cityDAO.findCityByName(selectedCity);
        System.out.println(city.getName());

        String uri = "http://api.timezonedb.com/v2/get-time-zone?key=TNTPMZHOU4ZK&format=json&by=position&lat="
                + city.getLatitude() + "&lng=" + city.getLongitude();

        URL url = new URL(uri);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "application/json");

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line + '\n');
        }
        String jsonString = stringBuilder.toString();
        JSONObject jsonObject = new JSONObject(jsonString);

        System.out.println(jsonObject.getString("formatted"));


        PrintWriter print = response.getWriter();
        String docType = "<!doctype html>\n";
        String output = docType + "<html>\n" + "<body background=http://adventureflight.ph/wp-content/uploads/2015/12/airplane-facts.jpg>"+"<head><title> Timezone for selected city </title></head>\n" + "<body>\n"
                + "<table><tr><th>City</th><th>Latitude</th><th>Longitude</th>"
                + "<th>Time</th></tr>";

        String data = "<tr><td>" + selectedCity + "</td>" + "<td>" + city.getLatitude() + "</td>"
                + "<td>" + city.getLongitude() + "</td>" + "<td>" + jsonObject.getString("formatted") + "</td></tr>";
        output += data;

        output += "</table></body></html>";
        print.println(output);

        Client client = ClientBuilder.newClient();

        WebTarget target = client.target("https://api.elsevier.com/content/search/author?query=authlast(Sebestyen)")
                .queryParam("apiKey", "89c899dd469162464927aca2f96371a4");
        System.out.println(target.request(MediaType.APPLICATION_JSON).get().readEntity(String.class));


        //////SCIENCE DIRECT------------
        Client clientScDir = ClientBuilder.newClient();

        WebTarget targetScDir = clientScDir.target("https://api.elsevier.com/content/search/scidir?query=gheorghe+sebestyen")
                .queryParam("apiKey", "89c899dd469162464927aca2f96371a4");
        System.out.println(targetScDir.request(MediaType.APPLICATION_JSON).get().readEntity(String.class).toString());

    }
}
