package business;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import persistence.DAO.UserDAOImpl;
import persistence.model.User;

public class LoginServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		User user = new User();

		user.setUsername(request.getParameter("Username"));
		user.setPassword(request.getParameter("Password"));

		response.setContentType("text/html");
		HttpSession session = request.getSession(true);

		try {
			persistence.DAO.HibernateConfigs.getConfig();
			UserDAOImpl userDAO = new UserDAOImpl();
			String userType = userDAO.findUserType(user.getUsername(), user.getPassword());
			session.setAttribute("type", userType);
			if (userType.equals("admin")) {
				ServletContext serverContext = getServletContext();
				response.sendRedirect(request.getContextPath() + "/admin.html");
			} else {
				ServletContext serverContext = getServletContext();
				response.sendRedirect(request.getContextPath() + "/user.html");
			}

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

}
