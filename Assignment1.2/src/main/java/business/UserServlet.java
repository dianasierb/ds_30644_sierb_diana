package business;

import persistence.DAO.FlightDAOImpl;
import persistence.model.Flight;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class UserServlet extends HttpServlet{

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        FlightDAOImpl flightDAO = new FlightDAOImpl();
        List<Flight> listOfFlights = flightDAO.findAllFlights();

        PrintWriter print = response.getWriter();
        String docType = "<!doctype html>\n";
        String output = docType + "<html>\n" + "<body background=http://adventureflight.ph/wp-content/uploads/2015/12/airplane-facts.jpg>"+"<head><title> All flights </title></head>\n"+ "<body>\n"
                + "<table><tr><th>Airplane type</th><th>Departure City</th><th>Arival City</th>"
                + "<th>Departure Date</th><th>Arrival Date</th></tr>";


        for (Flight flight:
             listOfFlights) {
            String airplaneType = flight.getAirplaneType();
            String arivalCity = flight.getArrivalCity().getName();
            String departureCity = flight.getDepartureCity().getName();
            String arivalDateHour = flight.getArrivalDateHour();
            String departureDateHour = flight.getDepartureDateHour();
            String rows = "<tr><td>"+ airplaneType +"</td>" + "<td>"+ arivalCity +"</td>"
                    + "<td>"+ departureCity +"</td>" + "<td>"+ arivalDateHour +"</td>"
                    + "<td>"+ departureDateHour +"</td></tr>";
            output +=rows;
        }
        output += "</table></body></html>";
        print.println(output);
    }

}
