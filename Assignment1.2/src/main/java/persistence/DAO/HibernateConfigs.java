package persistence.DAO;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import persistence.model.City;
import persistence.model.Flight;
import persistence.model.User;

public class HibernateConfigs {
	public static SessionFactory factory;

	public static void getConfig() {
		try {
			factory = new Configuration().configure().addPackage("persistence.model"). // add package if used.
							addAnnotatedClass(User.class).addAnnotatedClass(City.class).addAnnotatedClass(Flight.class)
					.buildSessionFactory();
		} catch (Throwable ex) {
			System.err.println("Failed to create sessionFactory object." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

}
