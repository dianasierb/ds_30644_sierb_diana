package persistence.DAO;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import persistence.model.City;

import java.util.List;

import static persistence.DAO.HibernateConfigs.factory;

public class CityDAOImpl {

    public void addCity(City city) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(city);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
                e.printStackTrace();
            }

        } finally {
            session.close();
        }
    }

    public City findCityByName(String cityName) {
        City city = null;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            String queryString = "FROM persistence.model.City c WHERE c.name = :name";
            Query query = session.createQuery(queryString);
            query.setParameter("name", cityName);
            city = (City) query.uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return city;
    }

}
