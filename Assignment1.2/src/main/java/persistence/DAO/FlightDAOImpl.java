package persistence.DAO;

import static persistence.DAO.HibernateConfigs.factory;

import java.util.List;

import org.hibernate.*;

import persistence.model.Flight;

public class FlightDAOImpl {

	public Flight findFlightById(int id) {

		Flight flight = new Flight();
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
//			String queryString = "FROM persistence.model.Flight f WHERE f.id = :id";
//			Query query = session.createQuery(queryString);
//			query.setParameter("id", id);
//			List<Flight> listOfFlights = query.list();
//			flight = listOfFlights.get(0);
			flight = session.load(Flight.class, id);
			Hibernate.initialize(flight);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return flight;
	}

	public List<Flight> findAllFlights() {

		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			flights = session.createQuery("FROM persistence.model.Flight").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}

		} finally {
			session.close();
		}
		return flights;
	}

	public boolean deleteFlight(Flight flight) {

		System.out.println(flight.toString());
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Hibernate.initialize(flight);
			session.delete(flight);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}

		return true;
	}

	public void addFlight(Flight flight) {

		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(flight);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}

		} finally {
			session.close();
		}
	}

	public void update(Flight flight) {

		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(flight);
			Hibernate.initialize(flight);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

}
