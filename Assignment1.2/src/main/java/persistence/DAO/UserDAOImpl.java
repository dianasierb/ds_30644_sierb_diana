package persistence.DAO;

import static persistence.DAO.HibernateConfigs.factory;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import persistence.model.User;

public class UserDAOImpl {

	@SuppressWarnings("deprecation")
	public String findUserType(String username, String password) {
		User result = null;
		try {
			Session session = factory.openSession();
			Transaction tx = null;
			try {
				tx = session.beginTransaction();
				Query query = session.createQuery("From User where username=:username and password=:password");
				query.setString("username", username);
				query.setString("password", password);
				result = (User) query.uniqueResult();
			} catch (HibernateException e) {
				if (tx != null)
					tx.rollback();
				e.printStackTrace();
			} finally {
				session.close();
			}
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
		return result.getType();
	}

	public User findUserById(int id) {

		User user = null;

		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			user = (User) session.load(User.class, id);
			Hibernate.initialize(user);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return user;
	}

}
