package persistence.model;

import javax.persistence.*;

@Entity
@Table(name = "flight", schema = "assignment2")
public class Flight {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column
	private String airplaneType;

	@Column
	private String departureDateHour;

	@Column
	private String arrivalDateHour;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "departureCity", referencedColumnName = "city_id")
	private City departureCity;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "arrivalCity", referencedColumnName = "city_id")
	private City arrivalCity;

	public Flight(String airplaneType, String departureDateHour, String arrivalDateHour, City departureCity,
			City arrivalCity) {
		super();
		this.airplaneType = airplaneType;
		this.departureDateHour = departureDateHour;
		this.arrivalDateHour = arrivalDateHour;
		this.departureCity = departureCity;
		this.arrivalCity = arrivalCity;
	}

	public Flight() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAirplaneType() {
		return airplaneType;
	}

	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

	public String getDepartureDateHour() {
		return departureDateHour;
	}

	public void setDepartureDateHour(String departureDateHour) {
		this.departureDateHour = departureDateHour;
	}

	public String getArrivalDateHour() {
		return arrivalDateHour;
	}

	public void setArrivalDateHour(String arrivalDateHour) {
		this.arrivalDateHour = arrivalDateHour;
	}

	public City getDepartureCity() {
		return departureCity;
	}

	public void setDepartureCity(City departureCity) {
		this.departureCity = departureCity;
	}

	public City getArrivalCity() {
		return arrivalCity;
	}

	public void setArrivalCity(City arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

}
