package persistence.model;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "city", schema = "assignment2")
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "city_id")
    private int id;

    @Column
    private double latitude;

    @Column
    private double longitude;

    @Column
    private String name;

    @OneToMany(mappedBy = "departureCity", fetch = FetchType.EAGER)
    private List<Flight> departureFlights;

    @OneToMany(mappedBy = "arrivalCity", fetch = FetchType.EAGER)
    private List<Flight> arrivalFlights;

    public City(String name, double latitude, double longitude, List<Flight> departureFlights,
                List<Flight> arrivalFlights) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.departureFlights = departureFlights;
        this.arrivalFlights = arrivalFlights;
    }

    public City() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Flight> getDepartureFlights() {
        return departureFlights;
    }

    public void setDepartureFlights(List<Flight> departureFlights) {
        this.departureFlights = departureFlights;
    }

    public List<Flight> getArrivalFlights() {
        return arrivalFlights;
    }

    public void setArrivalFlights(List<Flight> arrivalFlights) {
        this.arrivalFlights = arrivalFlights;
    }

}
